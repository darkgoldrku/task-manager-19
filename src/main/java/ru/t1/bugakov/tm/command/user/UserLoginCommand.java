package ru.t1.bugakov.tm.command.user;

import ru.t1.bugakov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Login user.";
    }

}
