package ru.t1.bugakov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout user.";
    }

}
