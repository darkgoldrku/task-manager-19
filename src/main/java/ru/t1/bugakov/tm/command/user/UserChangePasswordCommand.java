package ru.t1.bugakov.tm.command.user;

import ru.t1.bugakov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

}
