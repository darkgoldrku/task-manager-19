package ru.t1.bugakov.tm.command.system;

import ru.t1.bugakov.tm.api.service.ICommandService;
import ru.t1.bugakov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }
}
