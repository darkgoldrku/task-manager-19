package ru.t1.bugakov.tm.command.user;

import ru.t1.bugakov.tm.api.service.IAuthService;
import ru.t1.bugakov.tm.api.service.IUserService;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public void showUser(final User user) {
        System.out.println("ID:" + user.getId());
        System.out.println("LOGIN:" + user.getLogin());
    }

}
