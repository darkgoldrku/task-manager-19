package ru.t1.bugakov.tm.exception.entity;

import ru.t1.bugakov.tm.model.AbstractModel;
import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.model.User;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error! Model not found.");
    }

}
