package ru.t1.bugakov.tm.api.comparator;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(final Date created);

}
