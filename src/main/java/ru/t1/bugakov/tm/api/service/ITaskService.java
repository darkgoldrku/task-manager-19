package ru.t1.bugakov.tm.api.service;


import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Task;


import java.util.List;

public interface ITaskService extends IAbstractService<Task> {

    Task create(String name, String description);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
