package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IAbstractRepository<Project> {

}
