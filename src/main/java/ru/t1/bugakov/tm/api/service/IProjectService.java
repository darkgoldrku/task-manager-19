package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.enumerated.Sort;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {


    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
