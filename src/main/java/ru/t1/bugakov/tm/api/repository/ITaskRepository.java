package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IAbstractRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
