package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}
